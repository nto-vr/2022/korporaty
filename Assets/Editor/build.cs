using UnityEditor;
using System.IO;

public class ViveCosmosBuilder
{
    public static void build(string arg)
    {
        string path = "Build/game.exe";
        if (!string.IsNullOrEmpty(arg))
        {
            path = Path.Combine(arg, "game.exe");
        }
        var scenes = new string[] 
        { 
            "Assets/Scenes/TestScene.unity"
        };

		BuildPipeline.BuildPlayer(scenes, path, BuildTarget.StandaloneWindows, BuildOptions.None); 

    }
} 