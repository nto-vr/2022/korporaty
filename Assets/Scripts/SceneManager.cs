﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SM = UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Threading;

public class SceneManager : MonoBehaviour
{
	[SerializeField]
	Image _progressBar;

	[SerializeField]
	Camera _camera;

	[SerializeField]
	string _scene;

	readonly float _fadeDuration = 1f;

	CanvasGroup _canvas;

	// Start is called before the first frame update
	void Start()
	{
		_canvas = GetComponent<CanvasGroup>();
		
		var f = _camera.transform.forward * 20;
		transform.position = f;
		transform.rotation = Quaternion.LookRotation(_camera.transform.position);
		StartCoroutine(Fade(1f, 0f));
		//StartCoroutine(LoadSceneAsync());
	}

	IEnumerator Fade(float start, float end)
	{
		float counter = 0f;
		while (counter < _fadeDuration)
		{
			counter += Time.deltaTime / 60;
			_canvas.alpha = Mathf.Lerp(start, end, counter);
		}
		yield return null;
	}


	// Update is called once per frame
	IEnumerator LoadSceneAsync()
	{
		AsyncOperation operation = SM.SceneManager.LoadSceneAsync(_scene);
		operation.allowSceneActivation = false;

		while (!operation.isDone)
		{
			_progressBar.fillAmount = operation.progress;

			if (operation.progress >= 0.9f)
			{
				
			}
			//Animation.
			Thread.Sleep(5000);

			

			operation.allowSceneActivation = true;

			_progressBar.fillAmount = 1f;
			yield return null;
		}
	}
}
