using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGun : MonoBehaviour
{
    public AudioSource audioS;
    public AudioClip clip_Fire;
    public AudioClip clip_NotBullets;
    public GameObject gun;
    private GameObject target;
    public GunShop gunshop;
    public GameObject boomVFX;
    public AudioSource boomSound;
    
    public void makeFire()
    {
        if(gunshop.BulletCount > 0)
        {
            audioS.clip = clip_Fire;
            audioS.Play();
            gunshop.BulletCount -= 1;
            gunshop.textBulletCount.text = gunshop.BulletCount.ToString();
            //уничтожаем метеор
            target=gun.GetComponent<GunLook>().MyTarget;
            Vector3 destroyObjPos=target.transform.position;
            Instantiate(boomVFX,destroyObjPos,Quaternion.identity).SetActive(true);
            Destroy(target);
            boomSound.Play();
            Debug.Log("Destroyed!");
        }
        else
        {
            audioS.clip = clip_NotBullets;
            audioS.Play();
        }
    }
}
