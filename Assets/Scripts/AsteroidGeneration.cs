using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGeneration : MonoBehaviour
{
	public List<GameObject> prefabs;

	[Space]

	public GameObject target;
	public GameObject radar;

	[Space]

	public AudioSource boomSource;
	public float interval = 3.0f;

	[Space]

	public Range xPos;
	public Range yPos;
	public Range zPos;

	private Radar _radar;
	private float _scaleR;
	private int _id;

	void Start()
	{
		_radar = radar.GetComponent<Radar>();
		StartCoroutine(TestCoroutine());
	}

	IEnumerator TestCoroutine()
	{
		while (true)
		{
			transform.position = RandomPos();

			yield return new WaitForSeconds(interval);

			Random.InitState(System.Environment.TickCount);
			int i = Random.Range(0, prefabs.Count);

			GameObject ast = Instantiate(prefabs[i], transform.position, Quaternion.identity);

			var am = ast.GetComponent<AsteroidMoving>();
			am.target = target.transform;
			am.radar = _radar;
			am.id = _id;
			_id++;

			_scaleR = Random.Range(-30.0f, 30.0f);
			ast.transform.localScale += new Vector3(_scaleR, _scaleR, _scaleR);
			ast.SetActive(true);
		}
	}

	Vector3 RandomPos()
	{
		float x, y, z;

		Random.InitState(System.Environment.TickCount * Random.Range(1, 3));
		x = Random.Range(xPos._min, xPos._max) * (Random.Range(0, 2) == 0 ? 1 : -1);

		Random.InitState(System.Environment.TickCount * Random.Range(1, 3));
		y = Random.Range(yPos._min, yPos._max);

		Random.InitState(System.Environment.TickCount * Random.Range(1, 3));
		z = Random.Range(zPos._min, zPos._max) * (Random.Range(0, 2) == 0 ? 1 : -1);

		return new Vector3(x, y, z);

	}

	[System.Serializable]
	public struct Range
	{
		[SerializeField]
		public float _min;

		[SerializeField]
		public float _max;

		public Range(float t1, float t2)
		{
			_min = t1;
			_max = t2;
		}
	}
}
