﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class MenuController : MonoBehaviour
{

    public SteamVR_Action_Boolean MenuPress = null;
    private bool _MenuIsOpen = false;
    [Space]
    [Space]
    public float MenuHeadDistance;
    [Space]
    public GameObject MenuObj;
    public GameObject head;

    void Start()
    {
        if(MenuObj == null)
        {
            Debug.LogWarning("MenuObj is Null");
        }
        if (head == null)
        {
            Debug.LogWarning("head is Null");
        }

        MenuObj.SetActive(false);
    }

    
    private void HideMunu()
    {
        _MenuIsOpen = false;

        MenuObj.SetActive(false);
    }
    private void OpenMenu()
    {
        _MenuIsOpen = true;

        MenuObj.SetActive(true);
        TransitionMenuToPlayerHead();
    }

    private void TransitionMenuToPlayerHead()
    {
        Vector3 newpos = head.transform.position + new Vector3(head.transform.forward.x,0, head.transform.forward.z) * MenuHeadDistance;

        MenuObj.transform.position = newpos;

        MenuObj.transform.LookAt(head.transform);
    }

    void Update()
    {
        //TransitionMenuToPlayerHead();
        if (MenuPress.GetStateDown(SteamVR_Input_Sources.Any))
        {
            if(!_MenuIsOpen)
            {
                OpenMenu();
            }
            else
            {
                HideMunu();
            }
            
        }
    }
}
