﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PereskopeScript : MonoBehaviour
{
    public GameObject VR_cam;
    public GameObject mainPlayer;

    [Space]
    public SteamVR_Action_Boolean PressLeavePereskope;

    private bool _inPerescope = false;

    public void ToPerescope()
    {
        _inPerescope = true;
        InputTracking.disablePositionalTracking = true;


        mainPlayer.SetActive(false);
        VR_cam.SetActive(true);
    }
    public void LeavePerescope()
    {
        _inPerescope = false;
        InputTracking.disablePositionalTracking = false;


        mainPlayer.SetActive(true);
        VR_cam.SetActive(false);
    }

    void Update()
    {
            if (PressLeavePereskope.GetStateDown(SteamVR_Input_Sources.Any))
            {
                LeavePerescope();
            }
    }
}
