﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GunShop : MonoBehaviour
{
    public Text textBulletCount;

    public int BulletCount;

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Rigidbody>())
        {
            BulletCount += 1;
            textBulletCount.text = BulletCount.ToString();

            Destroy(other.gameObject);
        }
    }
}
