﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SborkaGuns : MonoBehaviour
{

    public GameObject original;
    public GameObject waitObject;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == waitObject)
        {
            
            original.SetActive(true);
            Destroy(other.gameObject);
            gameObject.SetActive(false);
        }
    }
}
