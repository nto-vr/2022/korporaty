using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSensor : MonoBehaviour
{
	public GameObject boomVFX;
	public AudioSource sirena;
	public GameObject stats;

	[Space]

	public AudioSource boomSound;
	public AudioSource electroSound;
	public AudioSource EnergyKritical;

	[Space]
	public GameObject lightsRed;
	public GameObject allLight;

	[Space]

	public Material material;

	int _boe = 0;
	int _chanceOfLightBreak;
	GameStats _stats;

	// Start is called before the first frame update
	void Start()
	{
		_stats = stats.GetComponent<GameStats>();
		//lightsRed.GetComponent<Light>().enabled=false;
	}


	public void OnShield()
	{
		Debug.Log("Burst of energy");
		sirena.Stop();
		electroSound.Play();
		_boe = 1;

		//    - _Color: {r: 0, g: 0.4287176, b: 1, a: 0.40392157}
		//-_EmissionColor: { r: 0, g: 0.42745098, b: 1, a: 1}

		material.SetColor("_EmissionColor", new Color(0, 0.427f, 1, 1));
		material.color = new Color(0, 0.428f, 1, 0.403f);
		StartCoroutine(BOECOUR());
	}

	// Update is called once per frame
	//void Update()
	//{
	//    if (Input.GetKeyUp(KeyCode.F)&&boe==0) {
	//        Debug.Log("Burst of energy");
	//        sirena.Stop();
	//        electroSound.Play();
	//        boe=1;
	//        StartCoroutine(BOECOUR());
	//    }
	//}

	IEnumerator BOECOUR()
	{
		yield return new WaitForSeconds(5f);
		Debug.Log("BOF doned");
		_boe = 0;
		material.SetColor("_EmissionColor", new Color(0.211f, 0.405f, 0.167f, 1));
		material.color = new Color(0.182f, 1, 0, 0.403f);
	}

	void StopSirena()
	{
		sirena.Stop();
		lightsRed.SetActive(false);
	}
	void StartSirena()
	{
		sirena.Play();
		lightsRed.SetActive(true);
	}

	void OffAll()
	{
		allLight.SetActive(false);
		sirena.enabled = false;
		GetComponent<AudioSource>().enabled = false;
	}
	public void OnAll()
	{
		allLight.SetActive(true);
		sirena.enabled = true;
		GetComponent<AudioSource>().enabled = true;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<AsteroidMoving>())
		{
			if (_boe == 0)
			{ //щит не работает
				GetComponent<AudioSource>().Play();
				Invoke("StartSirena", GetComponent<AudioSource>().clip.length);
				Invoke("StopSirena", 10);

				//поломка света
				_chanceOfLightBreak = Random.Range(0, 100);
				if (_chanceOfLightBreak > 80)
				{
					EnergyKritical.Play();
					Debug.Log("switchBreaked");
					OffAll();
				}
			}
			else
			{ //если работает уничтожилка
				Vector3 destroyObjPos = other.gameObject.transform.position;
				Instantiate(boomVFX, destroyObjPos, Quaternion.identity).SetActive(true);
				Destroy(other.gameObject);
				boomSound.Play();
				Debug.Log("Destroyed!");
				_stats.AsteroidsDestroyed++;
			}

		}
	}

	private void OnTriggerStay(Collider other) //уничтожаем астероид на территории триггера
	{
		if (other.GetComponent<AsteroidMoving>() && _boe == 1)
		{
			Vector3 destroyObjPos = other.gameObject.transform.position;
			Instantiate(boomVFX, destroyObjPos, Quaternion.identity).SetActive(true);
			Destroy(other.gameObject);
			boomSound.Play();
			Debug.Log("Destroyed!");
			sirena.Stop();
			_stats.AsteroidsDestroyed++;
		}
	}
}
