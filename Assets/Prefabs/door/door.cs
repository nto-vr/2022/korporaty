using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class door : MonoBehaviour
{
	[SerializeField]
	VRContr2 _player;

	[SerializeField]
	GameObject _left;

	[SerializeField]
	GameObject _right;

	[SerializeField]
	bool _disabled = false;

	Vector3 _leftPos;
	Vector3 _rightPos;

	public bool _open = false;


	public float OpenDist;

	
	public AudioSource doorNorm;
	private Animator anim;

	// Start is called before the first frame update
	void Start()
	{
		anim = GetComponent<Animator>();


		_leftPos = _left.transform.localPosition;
		_rightPos = _right.transform.localPosition;

		if (!_disabled)
			Open();
	}

	// Update is called once per frame
	void Update()
	{
		if (!_disabled)
		{
			
			var d = Vector3.Distance(transform.position, _player.transform.position);
			if (!_open && d < OpenDist)
			{
				Open();
				if (!doorNorm.isPlaying) doorNorm.Play();
			}
			if (_open && d > OpenDist)
			{
				Close();
				if (!doorNorm.isPlaying) doorNorm.Play();
			}
		}
	}
	void Open()
	{    
        _open = true;
		anim.SetBool("IsOpen", true);
	}

	void Close()
	{
        _open = false;
		anim.SetBool("IsOpen", false);
	}

}
