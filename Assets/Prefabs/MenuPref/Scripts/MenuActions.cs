﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SM = UnityEngine.SceneManagement;

public class MenuActions : MonoBehaviour
{



    /// <summary>
    /// Restarts the current level
    /// </summary>
    public void RestartLevel()
    {

        SM.SceneManager.LoadScene(SM.SceneManager.GetActiveScene().name);
    }



    /// <summary>
    /// Closes the application
    /// </summary>
    public void ExitGame()
    {
        Application.Quit();
    }



    /// <summary>
    /// Changes the volume level in the game
    /// </summary>
    /// <param name="u"></param>
    public void SougnChange(float u)
    {
        ///pass
    }
}
