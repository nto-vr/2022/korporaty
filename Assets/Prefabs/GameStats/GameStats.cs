﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameStats : MonoBehaviour
{
	public Text text_time;
	public Text text_destroyed;
	public Text text_repairs;

	private int _seconds = 0;
	private int _destroyed = 0;
	private int _repairs = 0;


	public int AsteroidsDestroyed
	{
		get => _destroyed;
		set
		{
			_destroyed = value;
			text_destroyed.text = _destroyed.ToString();
		}
	}

	public int Repairs
	{
		get => _destroyed;
		set
		{
			_destroyed = value;
			text_repairs.text = _destroyed.ToString();
		}
	}

	// Start is called before the first frame update
	void Start()
	{
		StartCoroutine(UpdateTime());
	}

	IEnumerator UpdateTime()
	{
		while (true)
		{

			yield return new WaitForSeconds(1f);
			_seconds++;
			text_time.text = $"{_seconds}s";
		}
	}

	// Update is called once per frame
	void Update()
	{

	}
}
