﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Fixdoor : MonoBehaviour
{
    public SteamVR_Action_Boolean PressFix;

    public ParticleSystem particles;

    public bool fixnow = false;
    // Update is called once per frame
    void Update()
    {
        if (PressFix.GetStateDown(SteamVR_Input_Sources.Any))
        {

            particles.Play();

            fixnow = true;
        }
        else
        {
            fixnow = false;
        }
    }
}
