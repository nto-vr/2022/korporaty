﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastAsteroid : MonoBehaviour
{

    public LayerMask layerMask;


    public GameObject TargetCanvas;

    public GameObject Gun;

    public GameObject LastTarg;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TargetAsteroid();
    }
    private void FixedUpdate()
    {
        
    }


    void TargetAsteroid()
    {
        RaycastHit hit;
        
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);

            if (hit.collider.gameObject.GetComponent<AsteroidMoving>())
            {
                Debug.Log("hittt");
                if(LastTarg != null)
                {
                    Destroy(LastTarg);
                }

                LastTarg = Instantiate(TargetCanvas, hit.collider.gameObject.transform);




                Gun.GetComponent<GunLook>().MyTarget = hit.collider.gameObject;
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
        }
    }
}
