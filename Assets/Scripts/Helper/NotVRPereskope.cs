using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotVRPereskope : MonoBehaviour
{ 
	private Vector3 startEulerAngles;
	private Vector3 startMousePosition;
	private float realTime;
    public GameObject mainPlayer;
    public GameObject VR_cam;

	public bool OnDebug = true;

	void Start()
	{
		gameObject.GetComponent<NotVRPereskope>().enabled = OnDebug;
	}

	//-------------------------------------------------
	void OnEnable()
	{
		realTime = Time.realtimeSinceStartup;
	}


	//-------------------------------------------------
	void Update()
	{
		
		float realTimeNow = Time.realtimeSinceStartup;
		float deltaRealTime = realTimeNow - realTime;
		realTime = realTimeNow;

		Vector3 mousePosition = Input.mousePosition;

		if (Input.GetMouseButtonDown(1) /* right mouse */)
		{
			startMousePosition = mousePosition;
			startEulerAngles = transform.localEulerAngles;
		}

		if (Input.GetMouseButton(1) /* right mouse */)
		{
			Vector3 offset = mousePosition - startMousePosition;
			transform.localEulerAngles = startEulerAngles + new Vector3(-offset.y * 360.0f / Screen.height, offset.x * 360.0f / Screen.width, 0.0f);
		}
		if (Input.GetKeyDown(KeyCode.Escape)) {
            mainPlayer.SetActive(true);
            VR_cam.SetActive(false);
		}
	}
}
