﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenDoor : MonoBehaviour
{
    public bool IsBroken = false;

    public GameObject infocan;

    public void Breakdoor()
    {
        GetComponent<Animator>().SetBool("IsOpen", false);
        GetComponent<AudioSource>().mute = false;
        GetComponent<door>().enabled = false;
        infocan.SetActive(true);
    }
    public void Fixdoor()
    {
        GetComponent<AudioSource>().mute = true;
        GetComponent<door>().enabled = true;
        infocan.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<VRContr2>())
        {
            if (Random.Range(0, 5) == 0)
            {
                IsBroken = true;
                Breakdoor();
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Fixdoor>())
        {
                IsBroken = false;

            if (other.GetComponent<Fixdoor>().fixnow)
            {
                Fixdoor();
            }
        }
    }
}
