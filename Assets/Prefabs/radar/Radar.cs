﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radar : MonoBehaviour
{
	public GameObject dotObject;

	[System.NonSerialized]
	public AsteroidMoving[] Asteroids = new AsteroidMoving[1000];

	[System.NonSerialized]
	public GameObject[] Dots = new GameObject[1000];

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		StartCoroutine(UpdateLocation());
	}


	IEnumerator UpdateLocation()
	{
		while (true)
		{
			yield return new WaitForSeconds(1f);

			for (int i = 0; i < 1000; i++)
			{
				if (Asteroids[i] is AsteroidMoving asteroid)
				{
					var dis = Vector3.Distance(transform.position, asteroid.gameObject.transform.position);

					if (dis < 220 && dis > 0)
					{
						var p = asteroid.gameObject.transform.position;
						var s = asteroid.speed * 0.01d;

						double time = dis / s;

						var c = 350;

						var local = new Vector3(p.x / c, p.y / c, p.z / c) + transform.position;

						if (Dots[i] is GameObject dot)
						{
							dot.transform.position = local;
							//dot.GetComponent<Dot>().text.text = $"{System.Math.Round(time, 1)}s";
						}
						else
						{
							var obj = Instantiate(dotObject, Vector2.zero, Quaternion.identity);
							obj.transform.position = local;
							obj.SetActive(true);

							Dots[i] = obj;
							//obj.GetComponent<Dot>().text.text = $"{System.Math.Round(time, 1)}s";
						}
					}
				}
				else
				{
					if (Dots[i])
					{
						var o = Dots[i];
						Dots[i] = null;
						Destroy(o);
					}
				}
			}
		}
	}
}
