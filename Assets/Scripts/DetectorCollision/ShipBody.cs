using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBody : MonoBehaviour
{

    public int popadani = 0;

    public MiniShipVisual miniShip;
    public GameObject player;
    public AudioSource bahsong;
    public GameObject boomVFX;
    public GameObject allScene;
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.GetComponent<AsteroidMoving>())
        {
            popadani += 1;
            bahsong.Play();

            miniShip.addDamage();

            
            Vector3 destroyObjPos = other.gameObject.transform.position;
            Instantiate(boomVFX, destroyObjPos, Quaternion.identity).SetActive(true);
            Destroy(other.gameObject);
            
            if (popadani==3) {
            //кораблю крышка
                player.transform.position=new Vector3(577.6376f,418.495f,-553.2259f);
                allScene.SetActive(false);
            }
        }
    }
}
