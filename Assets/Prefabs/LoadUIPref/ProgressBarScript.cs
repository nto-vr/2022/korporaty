﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SM = UnityEngine.SceneManagement;

public class ProgressBarScript : MonoBehaviour
{

    public Image Progressbar;
    public Text text;


    static public int s_nextSceneID = 1;

    public float status = 0f;
    public bool isStop = false;
    void update_bar()
    {
        Progressbar.fillAmount = status;

        text.text = (status * 100).ToString();
    }



    
    void Update()
    {
        if (!isStop)
        {

            update_bar();
            status += Time.deltaTime;



            if (status >= 1)
            {
                if (s_nextSceneID != -1)
                {
                    SM.SceneManager.LoadScene(ProgressBarScript.s_nextSceneID);
                }
                isStop = true;
            }
        }
    }
}
