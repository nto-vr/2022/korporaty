# Space Simulator VR

Our project that was created at [NTI 2022](https://ntcontest.ru/)

## UNITY VERSION

2019.4.36f1

## HEADSET

Vive Cosmos

## HOW TO BUILD

execute this commands into cmd.

don't forget to replace ```path_to_project``` and ```path_to_build``` with real path.

if you make ```path_to_build``` empty, project will be built into "```path_to_project```/Build" folder

```shell
cd "path_to_project"
```
```shell
"C:\Program Files\2019.4.36f1\Editor\Unity.exe" -quit -batchmode -projectPath . -executeMethod ViveCosmosBuilder.build "path_to_build"
```

## CODE CONVENTIONS

official naming conventions from [Microsoft Docs](https://docs.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions)

## GAME CONRTOL

![](photo_2022-03-01_20-37-18.jpg)
