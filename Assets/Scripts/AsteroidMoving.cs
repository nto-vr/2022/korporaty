using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMoving : MonoBehaviour
{
	[System.NonSerialized]
	public Transform target;

	[System.NonSerialized]
	public int id;

	[System.NonSerialized]
	public Radar radar;

	public int speed = 8;
	public int rotSpeed = 15;

	private int _randomSpeed = 0;
    public Material coldMat;
    public Material normalMat;
	private void Start()
	{
		StartCoroutine(PostData());
	}

	void Awake()
	{
		_randomSpeed = Random.Range(-2, 5);
		speed += _randomSpeed;
	}

	IEnumerator PostData()
	{
		while (true)
		{
			yield return new WaitForSeconds(1f);

			radar.Asteroids[id] = this;
		}
	}

	private void OnDestroy()
	{
		radar.Asteroids[id] = null;
	}
	void Update()
	{  
        if (asteroidFreeze.freezing==0) {
        gameObject.GetComponent<MeshRenderer>().material=normalMat;
		transform.position = Vector3.MoveTowards(transform.position, target.position, 0.01f * speed);
		if (transform.position == target.position)
			Destroy(this.gameObject);

		transform.Rotate(Vector3.right * (Time.deltaTime * rotSpeed));
		transform.Rotate(Vector3.down * (Time.deltaTime * rotSpeed));
		}
		else {
            gameObject.GetComponent<MeshRenderer>().material=coldMat;
		}
	}
}
